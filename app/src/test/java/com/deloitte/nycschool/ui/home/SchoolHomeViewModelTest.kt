package com.deloitte.nycschool.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.deloitte.nycschool.model.SchoolItem
import com.deloitte.nycschool.network.NetworkResult
import com.deloitte.nycschool.repository.HomeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolHomeViewModelTest {

    private lateinit var schoolHomeViewModel: SchoolHomeViewModel
    private lateinit var repository: HomeRepository

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(Dispatchers.Unconfined)

        repository = mock(HomeRepository::class.java)
        schoolHomeViewModel = SchoolHomeViewModel((repository))
    }

    @Test
    fun getSchoolListSuccessTest() = runBlocking {
        val schoolList = listOf(
            SchoolItem("12ABC3", school_name = "School 1"),
            SchoolItem("12XYZ3", school_name = "School 2")
        )

        `when`(repository.fetchNycSchools()).thenReturn(
            NetworkResult.Success(
                schoolList
            )
        )

        schoolHomeViewModel.getSchoolListData()

        assertEquals(schoolList.size, schoolHomeViewModel.homeUiState.value?.schools?.size)
    }

    @Test
    fun getSchoolListErrorTest() = runBlocking {

        `when`(repository.fetchNycSchools()).thenReturn(
            NetworkResult.Error(
                responseBodyData = "Api call failed",
                responseCode = 500
            )
        )

        schoolHomeViewModel.getSchoolListData()

        assertEquals(SchoolHomeUiState.Error, schoolHomeViewModel.homeUiState.value)
        assertEquals(0, schoolHomeViewModel.homeUiState.value?.schools?.size)
    }
}