package com.deloitte.nycschool.network

/**
 * [NetworkResult] to hold data for any response inclduing success data or error
 *
 * @property data
 * @property responseBodyData
 * @property responseCode
 */
sealed class NetworkResult<T>(
    val data: T? = null,
    val responseBodyData: Any? = null,
    val responseCode: Int? = null
) {
    class Success<T>(data: T) : NetworkResult<T>(data)
    class Error<T>(responseBodyData: Any, responseCode: Int?) :
        NetworkResult<T>(null, responseBodyData, responseCode)
}
