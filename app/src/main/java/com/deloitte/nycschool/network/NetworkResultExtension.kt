package com.deloitte.nycschool.network

/**
 * This are the extensions for [NetworkResult] to handle success and error easily
 *
 * @param T
 * @param action
 * @return
 */
inline fun <T : Any> NetworkResult<T>.onSuccess(action: (T) -> Unit): NetworkResult<T> {
    if (this is NetworkResult.Success) {
        this.data?.let { action(it) }
    }
    return this
}

inline fun <T : Any> NetworkResult<T>.onError(action: (NetworkResult.Error<*>) -> Unit): NetworkResult<T> {
    if (this is NetworkResult.Error<*>) {
        action(this)
    }
    return this
}
