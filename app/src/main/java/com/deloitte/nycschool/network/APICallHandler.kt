package com.deloitte.nycschool.network

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * This helps us to make API call and handle multiple cases
 *
 */
suspend fun <T : Any> safeApiCall(
    dispatcher: CoroutineDispatcher = Dispatchers.IO,
    apiCall: suspend () -> T
): NetworkResult<T> {
    return withContext(dispatcher) {
        try {
            NetworkResult.Success(apiCall.invoke())
        } catch (e: Exception) {
            error(e.message ?: e.toString())
        }
    }
}

private fun <T> error(errorMessage: String, responseCode: Int? = null): NetworkResult<T> =
    NetworkResult.Error(
        responseBodyData = "Api Failure $errorMessage",
        responseCode = responseCode
    )

