package com.deloitte.nycschool.di

import com.deloitte.nycschool.ui.details.SchoolDetailsViewModel
import com.deloitte.nycschool.ui.home.SchoolHomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * DI - All ViewModel will be added in this Module
 */
val viewModelModule = module {
    viewModel { SchoolHomeViewModel(get()) }
    viewModel { SchoolDetailsViewModel(get()) }
}
