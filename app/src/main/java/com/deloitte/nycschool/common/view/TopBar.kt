package com.deloitte.nycschool.common.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.deloitte.nycschool.R
import com.deloitte.nycschool.navigation.SchoolRoute
import com.deloitte.nycschool.ui.theme.Typography

/**
 * TopBar for activity
 *
 * @param navController
 */
@Composable
fun TopBar(navController: NavHostController) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentScreen = navBackStackEntry?.destination

    when (currentScreen?.route) {
        SchoolRoute.Home.route -> {
            TitleBar(stringResource(SchoolRoute.Home.title), false)
        }

        SchoolRoute.SchoolDetails.route -> {
            TitleBar(stringResource(SchoolRoute.SchoolDetails.title), true, onBackClick = {
                if (navController.previousBackStackEntry != null) {
                    navController.navigateUp()
                }
            })
        }
    }
}


/**
 * TitleBar/Action bar with dynamic configuration like title , back button visibility
 *
 * @param title
 * @param showBack
 * @param onBackClick
 */
@Composable
fun TitleBar(title: String, showBack: Boolean, onBackClick: (() -> Unit)? = null) {
    TopAppBar {
        Box(modifier = Modifier.fillMaxWidth()) {
            if (showBack) {
                IconButton(
                    modifier = Modifier.align(Alignment.CenterStart),
                    onClick = { onBackClick?.invoke() }
                ) {
                    Image(
                        painterResource(id = R.drawable.ic_back),
                        contentDescription = stringResource(id = R.string.content_desc_back_icon)
                    )
                }
            }

            Text(
                text = title,
                style = Typography.h5,
                modifier = Modifier.align(Alignment.Center)
            )

        }
    }
}