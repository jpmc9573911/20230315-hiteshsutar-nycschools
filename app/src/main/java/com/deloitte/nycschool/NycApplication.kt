package com.deloitte.nycschool

import android.app.Application
import com.deloitte.nycschool.di.networkModule
import com.deloitte.nycschool.di.repositoryModule
import com.deloitte.nycschool.di.servicesModule
import com.deloitte.nycschool.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.component.KoinComponent
import org.koin.core.context.GlobalContext.startKoin

class NycApplication : Application(), KoinComponent {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@NycApplication)
            modules(
                viewModelModule,
                servicesModule,
                repositoryModule,
                networkModule
            )
        }
    }
}