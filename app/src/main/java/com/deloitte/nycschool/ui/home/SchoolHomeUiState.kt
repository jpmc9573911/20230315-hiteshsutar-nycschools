package com.deloitte.nycschool.ui.home

import com.deloitte.nycschool.model.SchoolItem

/**
 * Class to handle School List Screen different states
 *
 * @property schools
 */
sealed class SchoolHomeUiState(
    var schools: List<SchoolItem> = listOf()
) {
    object Loading : SchoolHomeUiState()
    object Error : SchoolHomeUiState()
    class Success(schools: List<SchoolItem>) : SchoolHomeUiState(
        schools = schools
    )
}
