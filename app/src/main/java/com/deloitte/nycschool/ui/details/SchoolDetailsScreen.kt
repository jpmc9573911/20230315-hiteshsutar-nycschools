package com.deloitte.nycschool.ui.details

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.deloitte.nycschool.R
import com.deloitte.nycschool.common.isInternetAvailable
import com.deloitte.nycschool.common.view.AppProgressBar
import com.deloitte.nycschool.common.view.ErrorScreen
import com.deloitte.nycschool.model.SchoolItem
import org.koin.androidx.compose.getViewModel

/**
 * Composable to display all Schools Details
 *
 * @param schoolBasicData
 */
@Composable
fun SchoolDetailsScreen(schoolBasicData: SchoolItem) {
    val schoolDetailsViewModel = getViewModel<SchoolDetailsViewModel>()

    val homeDetailsUiState by schoolDetailsViewModel.homeDetailsUiState.observeAsState(
        SchoolDetailsUiState.Loading
    )
    val context = LocalContext.current

    when (homeDetailsUiState) {
        SchoolDetailsUiState.Error -> {
            ErrorScreen {
                if (isInternetAvailable(context))
                    schoolDetailsViewModel.getSchoolDetails(schoolBasicData.dbn.toString())
                else
                    Toast.makeText(
                        context,
                        context.getText(R.string.internet_connection_warning),
                        Toast.LENGTH_SHORT
                    ).show()
            }
        }
        SchoolDetailsUiState.Loading -> {
            AppProgressBar()
        }
        is SchoolDetailsUiState.Success -> {
            Column(
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .fillMaxWidth()
            ) {
                SchoolGeneralDetails(schoolBasicData = schoolBasicData)
                SchoolSatDetails(schoolSatDetails = homeDetailsUiState.schoolDetails)
                Divider(modifier = Modifier.padding(horizontal = 16.dp))
                SchoolContactsDetails(schoolBasicData = schoolBasicData)
            }
        }
    }

    LaunchedEffect(Unit) {
        schoolDetailsViewModel.getSchoolDetails(schoolBasicData.dbn.toString())
    }
}
