package com.deloitte.nycschool.ui.home

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import com.deloitte.nycschool.R
import com.deloitte.nycschool.common.isInternetAvailable
import com.deloitte.nycschool.common.view.AppProgressBar
import com.deloitte.nycschool.common.view.ErrorScreen
import com.deloitte.nycschool.model.SchoolItem
import com.deloitte.nycschool.ui.theme.Typography
import org.koin.androidx.compose.getViewModel

@Composable
fun SchoolHomeScreen(onListItemClick: ((SchoolItem)) -> Unit) {
    val homeViewModel = getViewModel<SchoolHomeViewModel>()
    val homeUiState by homeViewModel.homeUiState.observeAsState(SchoolHomeUiState.Loading)
    val context = LocalContext.current

    when (homeUiState) {
        SchoolHomeUiState.Error -> {
            ErrorScreen {
                if (isInternetAvailable(context))
                    homeViewModel.getSchoolListData()
                else
                    Toast.makeText(
                        context,
                        context.getText(R.string.internet_connection_warning),
                        Toast.LENGTH_SHORT
                    ).show()
            }
        }
        SchoolHomeUiState.Loading -> {
            AppProgressBar()
        }
        is SchoolHomeUiState.Success -> Column {
            SchoolList(homeUiState.schools, onListItemClick)
        }
    }
}

@Composable
fun SchoolList(schools: List<SchoolItem>, onListItemClick: (SchoolItem) -> Unit) {
    LazyColumn {
        items(schools.size) { index ->
            SchoolItem(schools[index], onListItemClick)
        }
    }
}

@Composable
fun SchoolItem(school: SchoolItem, onListItemClick: (SchoolItem) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp),
        shape = RoundedCornerShape(12.dp),
        elevation = 2.dp
    ) {
        val context = LocalContext.current
        Column(
            modifier = Modifier
                .clickable(onClick = {
                    if (isInternetAvailable(context))
                        onListItemClick.invoke(school)
                    else
                        Toast
                            .makeText(
                                context,
                                context.getText(R.string.internet_connection_warning),
                                Toast.LENGTH_SHORT
                            )
                            .show()
                })
                .padding(16.dp)
        ) {
            Text(
                text = school.school_name.toString(),
                style = Typography.h6,
                modifier = Modifier.padding(bottom = 8.dp)
            )
            Row(verticalAlignment = Alignment.CenterVertically) {
                Image(
                    painter = painterResource(id = R.drawable.ic_address),
                    contentDescription = stringResource(
                        R.string.address
                    )
                )
                Spacer(Modifier.width(2.dp))
                Text(
                    text = "${school.city.toString()},",
                    style = Typography.body2
                )
                Spacer(Modifier.width(2.dp))
                Text(
                    text = school.zip.toString(),
                    style = Typography.body2,
                    fontStyle = FontStyle.Italic
                )

            }
        }
    }
}

