package com.deloitte.nycschool.ui.details

import com.deloitte.nycschool.model.SchoolSatItem

/**
 * Class to handle School Details Screen different states
 *
 * @property schoolDetails
 */
sealed class SchoolDetailsUiState(
    var schoolDetails: List<SchoolSatItem> = listOf()
) {
    object Loading : SchoolDetailsUiState()
    object Error : SchoolDetailsUiState()
    class Success(schools: List<SchoolSatItem>) : SchoolDetailsUiState(
        schoolDetails = schools
    )
}
