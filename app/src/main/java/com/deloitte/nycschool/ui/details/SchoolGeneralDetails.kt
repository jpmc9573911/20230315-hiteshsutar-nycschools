package com.deloitte.nycschool.ui.details

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.deloitte.nycschool.model.SchoolItem
import com.deloitte.nycschool.ui.theme.Typography

@Composable
fun SchoolGeneralDetails(
    schoolBasicData: SchoolItem,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Text(
            text = schoolBasicData.school_name.toString(),
            style = Typography.h5,
            modifier = Modifier.padding(bottom = 8.dp)
        )
        Text(
            text = schoolBasicData.overview_paragraph.toString(),
            style = Typography.body1,
            modifier = Modifier.padding(bottom = 8.dp),
            lineHeight = 20.sp
        )
    }
}
