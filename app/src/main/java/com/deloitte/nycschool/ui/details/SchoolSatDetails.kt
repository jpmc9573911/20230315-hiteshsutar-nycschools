package com.deloitte.nycschool.ui.details

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.deloitte.nycschool.R
import com.deloitte.nycschool.model.SchoolSatItem
import com.deloitte.nycschool.ui.theme.LightBlue
import com.deloitte.nycschool.ui.theme.Typography

@Composable
fun SchoolSatDetails(
    schoolSatDetails: List<SchoolSatItem>
) {
    if (schoolSatDetails.isNotEmpty()) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {

            Text(
                text = stringResource(id = R.string.sat),
                style = Typography.h6,
            )
            val schoolSat = schoolSatDetails[0]
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp)
            ) {
                ScoreBox(
                    label = stringResource(R.string.math),
                    score = schoolSat.sat_math_avg_score.toString(),
                    color = LightBlue,
                    modifier = Modifier.padding(end = 4.dp)
                )
                ScoreBox(
                    label = stringResource(R.string.writing),
                    score = schoolSat.sat_writing_avg_score.toString(),
                    color = LightBlue,
                    modifier = Modifier.padding(horizontal = 4.dp)
                )
                ScoreBox(
                    label = stringResource(R.string.reading),
                    score = schoolSat.sat_critical_reading_avg_score.toString(),
                    color = LightBlue,
                    modifier = Modifier.padding(start = 4.dp)
                )
            }

        }
    }
}

@Composable
fun ScoreBox(
    label: String,
    score: String,
    color: Color,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = label,
            style = Typography.caption,
            modifier = Modifier.padding(bottom = 4.dp)
        )
        Box(
            modifier = Modifier
                .height(56.dp)
                .width(56.dp)
                .background(color, CircleShape),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = score,
                style = MaterialTheme.typography.body1,
                color = Color.White
            )
        }
    }
}