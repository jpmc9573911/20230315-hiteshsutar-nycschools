package com.deloitte.nycschool.ui.details

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deloitte.nycschool.network.onError
import com.deloitte.nycschool.network.onSuccess
import com.deloitte.nycschool.repository.HomeRepository
import kotlinx.coroutines.launch

/**
 * ViewModel for handling business logic, api calling of School Details
 *
 * @property homeRepository
 */
class SchoolDetailsViewModel(
    private val homeRepository: HomeRepository
) :
    ViewModel() {
    var homeDetailsUiState: MutableLiveData<SchoolDetailsUiState> = MutableLiveData()
        private set

    fun getSchoolDetails(dbn: String) = viewModelScope.launch {
        homeDetailsUiState.postValue(SchoolDetailsUiState.Loading)

        homeRepository.fetchNycSchoolsDetails(dbn).onSuccess {
            homeDetailsUiState.postValue(
                SchoolDetailsUiState.Success(it)
            )
        }.onError {
            homeDetailsUiState.postValue(SchoolDetailsUiState.Error)
            Log.e("response error", "${it.responseBodyData}")
        }
    }
}