package com.deloitte.nycschool.navigation

/**
 * List of all navigation patterns used in app
 */
object DeepLink {
    const val SCHOOL_DETAILS = "myapp://schoolDetails?data={data}"
}
