package com.deloitte.nycschool.repository

import com.deloitte.nycschool.model.SchoolItem
import com.deloitte.nycschool.model.SchoolSatItem
import com.deloitte.nycschool.network.NetworkResult
import com.deloitte.nycschool.network.safeApiCall
import com.deloitte.nycschool.service.NycSchoolService

/**
 * Repository to handle NYC school API call
 *
 * @property nycSchoolService
 */
class HomeRepository(private val nycSchoolService: NycSchoolService) {

    suspend fun fetchNycSchools(): NetworkResult<List<SchoolItem>> {
        return safeApiCall {
            nycSchoolService.fetchSchools()
        }
    }

    suspend fun fetchNycSchoolsDetails(dbn: String): NetworkResult<List<SchoolSatItem>> {
        return safeApiCall {
            nycSchoolService.fetchSchoolDetails(dbn)
        }
    }
}